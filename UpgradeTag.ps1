
if ($args.Length -ne 1){

    $commandlineCount = $args.Length
    Write-Host "The commandline args count is "$commandlineCount" . You args length should be 1 ."
    return
}

$Verison = $args[0]
cd .
$CurRootPath = $PWD
$lastSlashIdx="$CurRootPath".LastIndexOf("\")

$ModuleName = "$CurRootPath".Substring($lastSlashIdx+1).Trim()
Write-Host "ModuleName : "$ModuleName

Write-Host "Delete the local tag that name is "$Verison" ."
git tag --delete $Verison

Write-Host "Delete the remote origin tag that name is "$Verison" ."
git push --delete origin $Verison

$TargetPath = "UnityProject/Assets/FunPlus/$ModuleName"
Write-Host "Make subtree for project that target relative path is "$TargetPath
git subtree split --prefix="$TargetPath" --branch upm

git tag $Verison upm
git push origin upm --tags

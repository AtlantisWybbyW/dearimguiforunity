﻿using UnityEngine;
using ImGuiNET;

public class Demo : MonoBehaviour
{
    void OnEnable()
    {
        ImGuiUn.Layout += OnLayout;
    }

    void OnDisable()
    {
        ImGuiUn.Layout -= OnLayout;
    }

    void OnLayout()
    {
        //ImGui.ShowDemoWindow();


        bool open = true;

        if (!ImGui.Begin("wyb", ref open, ImGuiWindowFlags.MenuBar))
        {
            ImGui.End();
            return;
        }


        if (ImGui.BeginMenuBar())
        {
            if (ImGui.BeginMenu("Menu0"))
            {
                ImGui.MenuItem("Main menu bar", null, true);
                ImGui.MenuItem("Console", null, true);
                ImGui.MenuItem("Log", null, true);
                ImGui.EndMenu();
            }


            //if (ImGui.BeginMenu("Menu1"))
            //{
            //    Debug.LogError("Menu1");
            //    ImGui.EndMenu();
            //}
            ImGui.EndMenuBar();
        }
        ImGui.End();
    }
}
